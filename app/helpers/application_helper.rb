module ApplicationHelper

    # Returns the Gravatar URL for the given user.
    def avatar_url_for(user)
        if user.profile.avatar.attached?
            return avatar_url(user.profile)
        end
        if user
            gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
            gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
            return gravatar_url
        end
        image_path("default_user.png")
    end

    def avatar_url(profile)
        if profile.avatar.attached?
            url_for(profile.avatar)
        end
    end

end
