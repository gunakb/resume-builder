class UsersController < ApplicationController

    def new
    end

    def create
        if params[:session][:password] == params[:session][:password_redundant]
            @user = User.new(name: params[:session][:name], email: params[:session][:email], password: params[:session][:password])
            @user.profile = Profile.new
            if @user.save
                log_in(@user)
                redirect_to(edit_path)
            else
                flash.now[:danger] = 'Invalid data. Try again with correct details'
                render :signup, status: :unprocessable_entity
            end
        else
            flash.now[:danger] = 'Passwords do not match'
            render :signup, status: :unprocessable_entity
        end
    end
end
