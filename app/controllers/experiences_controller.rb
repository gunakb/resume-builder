class ExperiencesController < ApplicationController
    def new
        current_user.profile.experiences.create
        flash[:info] = "New experience created"
        redirect_to edit_url
    end
end
