class ProjectsController < ApplicationController
    before_action :logged_in_user, only: [:new]
    def new
        proj = Experience.find_by(id: params[:expId]).projects.create
        # current_user.profile.experiences.params[:expId].projects.create
        flash[:info] = "New project created"
        redirect_to edit_url
    end
end
